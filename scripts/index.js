$(document).ready(function() {
    $("#deleteBtn").click(function() {
        let delSku = [];
        $(":checkbox:checked").each(function() {
           delSku.push($(this).next().find(".sku").text());
        });
        $.ajax({
            type: "POST",
            data: {skuList : JSON.stringify(delSku)},
            url: "api/delete.php",
            success: function(msg) {
               if (msg === "1") {
                   location.reload();
               } else {
                   Swal.fire({
                       title: 'Warning!',
                       text: 'An error occupied during this operation, please try later!',
                       icon: 'warning',
                       confirmButtonText: 'Continue'
                   }).then(function(){
                       location.reload();
                   });
               }
            }
        });
    });
});