$(document).ready(function(){
    $("#divSpec").hide();

    $("#typeSwitch").change(function() {
        switch ($("#typeSwitch").val()) {
            case "0": // >Type Switcher
                $("#divSpec").hide();
                break;
            case "1": // >DVD
                $("#labelSpec").text("Size (MB)");
                $("#infoSpec").text("Please provide capacity in MB");
                $("#divSpec").show();
                break;
            case "2": // >Book
                $("#labelSpec").text("Weight (KG)");
                $("#infoSpec").text("Please provide weight in KG");
                $("#divSpec").show();
                break;
            case "3": // >Furniture
                $("#labelSpec").text("Dimensions (HxWxL)");
                $("#infoSpec").text("Please provide dimensions in HxWxL format");
                $("#divSpec").show();
                break;
        }
    });

    $("#saveBtn").click(function () {
        let error = false;
        if (!$("#inputSKU").val()) {
            $("#errorSKU").text("Please, submit SKU!");
            error = true;
        } else {
            $("#errorSKU").text(" ");
        }

        if (!$("#inputName").val()) {
            $("#errorName").text("Please, submit Name!");
            error = true;
        } else {
            $("#errorName").text(" ");
        }

        if (!$("#inputPrice").val()) {
            $("#errorPrice").text("Please, submit Price!");
            error = true;
        } else {
            if (isNaN($("#inputPrice").val()))
            {
                $("#errorPrice").text("Please, provide the data of indicated type! (Use . instead , for price)");
                return false;
            } else {
                $("#errorPrice").text("");
            }
        }

        if ($("#typeSwitch").val() === "0") {
            $("#errorSwitch").text("Please, choose type!");
            error = true;
        } else {
            if (!$("#inputSpec").val()) {
                $("#errorSpec").text("Please, provide information about this product!");
                error = true;
            } else {
                $("#errorSpec").text(" ");
            }
            $("#errorSwitch").text(" ");
        }

        if (!error) {
            let info;
            let input = $("#inputSpec").val();
            switch ($("#typeSwitch").val()) {
                case "1": // >DVD
                    if (isNaN(input))
                    {
                        $("#errorSpec").text("Please, provide the data of indicated type!");
                        return false;
                    }
                    info = "Size: " + input + " MB";
                    break;
                case "2": // >Book
                    if (isNaN(input))
                    {
                        $("#errorSpec").text("Please, provide the data of indicated type!");
                        return false;
                    }
                    info = "Weight: " + input + " KG";
                    break;
                case "3": // >Furniture
                    let size = input.toLowerCase().split("x");

                    if (size.length === 3) {
                        for (i=0; i<3; i++) {
                            if (isNaN(size[i]) || parseInt(size[i]) === 0) {
                                $("#errorSpec").text("Please, provide the data of indicated type!");
                                return false;
                            }
                        }
                    } else {
                        $("#errorSpec").text("Please, provide the data of indicated type!");
                        return false;
                    }
                    info = "Dimension: " + input;
                    break;
            }
            let product = {sku:$("#inputSKU").val(), name:$("#inputName").val(), price:$("#inputPrice").val(), info:info};
            $.post( "api/create.php", product)
                .done(function( data ) {
                    if (data === "2") {
                        window.location.replace("./index.php");
                    } else if (data === "1") {
                        Swal.fire({
                            title: 'Error!',
                            text: 'Product with this SKU already exists!',
                            icon: 'error',
                            footer: 'Please change SKU to unique value!',
                            confirmButtonText: 'Continue'
                        })
                    }
                });
        }

    });

});