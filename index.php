<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Product List</title>
    <meta name="description" content="Scandiweb Junior Developer test">
    <meta name="author" content="Artjoms Grišajevs">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="style/index.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
    <header>
        <h3 style="display:inline-block">Product List</h3>
        <div class="btn-group">
            <a href="add.html"><button type="button" class="btn btn-outline-dark">ADD</button></a>
            <button type="button" class="btn btn-outline-dark" style="margin-left: 20px" id="deleteBtn">MASS DELETE</button>
        </div>
    </header>
    <hr>
    <div id="content">
        <div class="row row-cols-1 row-cols-md-4 g-4">
            <?php
            $servername = "localhost";
            $username = "id16632465_user";
            $password = "J|2iUW}Tn]UUd$&&";
            $database = "id16632465_appdb";
            $conn = new mysqli($servername, $username, $password, $database);

            $sql = "SELECT * FROM `products`";
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) {
                echo "<div class=\"co\" id=\"0\">\n
                        <div class=\"card\">\n
                            <div class=\"card-body\">\n
                                <input type=\"checkbox\" class=\"form-check-input\">\n
                                <div class=\"card-content\">\n 
                                    <h5 class=\"card-text sku\">".$row["sku"]."</h5>
                                    <h5 class=\"card-text\">".$row["name"]."</h5>
                                    <h5 class=\"card-text\">".$row["price"]."</h5>
                                    <h5 class=\"card-text\">".$row["info"]."</h5>
                                </div>
                            </div>        
                        </div>
                      </div>";
            }
            ?>
        </div>
    </div>
    <hr>
    <footer>
        <h5 align="center">Scandiweb Test assigment</h5>
    </footer>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    <script src="scripts/index.js"></script>
</body>
</html>


